CREATE DATABASE pes;
\c pes

CREATE TABLE grupos(
	id INT PRIMARY KEY,
	grado VARCHAR(1),
	grupo VARCHAR(1),
	promedio FLOAT
);

CREATE TABLE materias(
	id INT PRIMARY KEY,
	nombre TEXT
);

CREATE TABLE maestros(
	id INT PRIMARY KEY,
	nombre TEXT,
	telefono TEXT,
	correo TEXT
);

CREATE TABLE maestros_materias(
	id_mat INT,
	id_maes INT,
	FOREIGN KEY (id_mat) REFERENCES materias(id),
	FOREIGN KEY (id_maes) REFERENCES maestros(id)
);

CREATE TABLE alumnos(
	no_control INT PRIMARY KEY,
	nombre TEXT,
	f_nac DATE,
	promedio FLOAT,
	id_gpo INT,
	FOREIGN KEY (id_gpo) REFERENCES grupos(id)
);

CREATE TABLE calificaciones(
	id INT PRIMARY KEY,
	calificacion FLOAT,
	conducta FLOAT,
	id_alu INT,
	id_mat INT,
	FOREIGN KEY (id_alu) REFERENCES alumnos(no_control),
	FOREIGN KEY (id_mat) REFERENCES materias(id)	
);